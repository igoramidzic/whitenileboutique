import { Injectable } from '@angular/core';
import { AngularFirestore } from '../../../node_modules/angularfire2/firestore';
import { BehaviorSubject } from '../../../node_modules/rxjs';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  catalog: BehaviorSubject<Item> = new BehaviorSubject(null);

  constructor(private afs: AngularFirestore) {
    this.afs.collection('inventory').ref.get()
      .then(catalog => {
        let items: any = catalog.docs.map(a => {
          const data = a.data() as Item;
          const id = a.ref.id;
          return { id, ...data };
        });
        this.catalog.next(items);
      })
  }

  getOneItem (itemID) {
    return new Promise((resolve, reject) => {
      this.afs.doc('inventory/' + itemID).ref.get()
        .then(res => {
          resolve({ ...res.data(), id: res.id });
        })
        .catch(error => {
          reject(error);
        });
    })
  }
}
