import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ItemModalComponent } from '../components/item-modal/item-modal.component';

@Injectable()
export class ModalService {

	constructor(private myModals: MatDialog) { }

	openQuickPreviewItemModal (item) {
		const quickPreviewItemModalRef = this.myModals.open(ItemModalComponent, {
			disableClose: false,
			data: { item }
		});

		return quickPreviewItemModalRef;
	}

}
