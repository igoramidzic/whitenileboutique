import { Injectable } from '@angular/core';
import { AngularFirestore } from '../../../node_modules/angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class StoreInfoService {

  storeInfo: any;

  constructor(private afs: AngularFirestore) {
    this.afs.doc('store-data/ge9T6dAosOTarvxazxga').ref.get()
      .then(res => {
        this.storeInfo = res.data();
      })
      .catch(error => {
        console.log(error.message);
      })

// // Set store address
//     this.afs.doc('store-data/ge9T6dAosOTarvxazxga').ref.update({
//       'address': `1890 W Bay Dr w2
// Largo, FL 33770`
//     })
  }
}
