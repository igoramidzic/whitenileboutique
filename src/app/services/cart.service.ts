import { Injectable } from '@angular/core';
import { Item } from '../models/item';
import { BehaviorSubject } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cartItems: Item[];
  cart: BehaviorSubject<Item[]> = new BehaviorSubject(null);

  constructor() {
    this.cartItems = [];
  }

  addItemToCart (item, size, quantity) {
    item.size = size;
    item.quantity = quantity;
    this.cartItems.unshift(item);
    this.cart.next(this.cartItems);
  }

  updateCartItemQuantity (index, quantity) {
    if (this.cartItems[index].quantity < 10 && quantity > 0) {
      return this.cartItems[index].quantity += quantity;
    }

    if (this.cartItems[index].quantity > 1 && quantity < 0) {
      return this.cartItems[index].quantity += quantity;
    }
  }

  removeItemFromCart (index) {
    this.cartItems.splice(index, 1);
    this.cart.next(this.cartItems);
  }
}
