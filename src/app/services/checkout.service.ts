import { Injectable } from '@angular/core';
import { AngularFirestore } from '../../../node_modules/angularfire2/firestore';
import { Item } from '../models/item';
import { Shipping } from '../models/shipping';
import { Payment } from '../models/payment';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private afs: AngularFirestore) { }

  placeOrder (items: Item[], contactInfo: Contact, shippingInfo: Shipping, paymentInfo: Payment) {
    let order = { items, contactInfo, shippingInfo, paymentInfo };

    return new Promise((resolve, reject) => {
      this.afs.collection('orders').add(order)
        .then(res => resolve(res))
        .catch(error => reject(error));
    });
  }

}
