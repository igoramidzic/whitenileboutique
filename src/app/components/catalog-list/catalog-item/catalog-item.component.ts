import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../models/item';
import { ModalService } from '../../../services/modal.service';
import { Router } from '../../../../../node_modules/@angular/router';
import { SlugifyPipe } from '../../../../../node_modules/ngx-pipes';

@Component({
  selector: 'app-catalog-item',
  templateUrl: './catalog-item.component.html',
  styleUrls: ['./catalog-item.component.scss']
})
export class CatalogItemComponent implements OnInit {

  @Input('item') item: Item;

  constructor(private modalService: ModalService, private router: Router,
              private slugifyPipe: SlugifyPipe) { }

  ngOnInit() {
  }

  openQuickPreviewItemModal () {
    this.modalService.openQuickPreviewItemModal(this.item);
  }

  onNavToItem () {
    this.router.navigate(
			['catalog', 'product'],
			{ queryParams: { name: this.slugifyPipe.transform(this.item.name), id: this.item.id } }
    );
  }

}
