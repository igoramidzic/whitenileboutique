import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from '../../services/catalog.service';
import { Item } from '../../models/item';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.scss']
})
export class CatalogListComponent implements OnInit {

  catalog: any[];
  filteredCatalog: any[];

  constructor(private catalogService: CatalogService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.catalogService.catalog.subscribe((res: any) => {
      this.catalog = res;
      this.filteredCatalog = res;

      if (this.catalog) {
        this.route.queryParams.subscribe(params => {
          this.filterInventory(params['category'], params['gender']);
        })
      }
    })


  }

  filterInventory (category, gender) {
    this.filteredCatalog = this.catalog
      .filter(item => {
        if (category) {
          return item.category.toLowerCase() === category.toLowerCase();
        }
        return true;
      })
      .filter(item => {
        if (gender) {
          return item.gender.toLowerCase() === gender.toLowerCase();
        }
        return true;
      });
	}

}
