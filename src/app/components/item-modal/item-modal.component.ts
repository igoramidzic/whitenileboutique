import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-item-modal',
  templateUrl: './item-modal.component.html',
  styleUrls: ['./item-modal.component.scss']
})
export class ItemModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ItemModalComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
