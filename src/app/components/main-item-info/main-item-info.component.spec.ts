import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainItemInfoComponent } from './main-item-info.component';

describe('MainItemInfoComponent', () => {
  let component: MainItemInfoComponent;
  let fixture: ComponentFixture<MainItemInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainItemInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainItemInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
