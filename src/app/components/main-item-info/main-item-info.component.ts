import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../models/item';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { MatDialog } from '../../../../node_modules/@angular/material';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-main-item-info',
  templateUrl: './main-item-info.component.html',
  styleUrls: ['./main-item-info.component.scss']
})
export class MainItemInfoComponent implements OnInit {

  @Input('item') item: Item;
  size: string;
  noSizeErrorMsg: boolean = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private myModal: MatDialog,
              private cartService: CartService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((query) => {
			if (!this.item.sizes.length) {
				this.size = 'One size';
			}
    })
  }

  addToCart () {
    if (this.size) {
      this.cartService.addItemToCart(this.item, this.size, 1);
      // After setting item in cart:
      this.myModal.closeAll();
      this.router.navigate(['cart']);
    }
	}

}
