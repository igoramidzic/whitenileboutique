import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { SlugifyPipe } from 'ngx-pipes';
import { StoreInfoService } from '../../services/store-info.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  categories: string[];
  genders: string[];
  showCategories: boolean = true;
  showGender: boolean = false;

  constructor(public route: ActivatedRoute,
							private router: Router,
              private slugifyPipe: SlugifyPipe,
              public storeInfoService: StoreInfoService) { }

  ngOnInit() {
    this.categories = [ 'dresses', 'shirts', 'jackets', 'bottoms', 'pants', 'shorts' ];
    this.genders = [ 'male', 'female' ];
  }

  onChangeCategory (category: string) {
		let categorySlug = this.slugifyPipe.transform(category);
		this.router.navigate(['/catalog'], {
      queryParams: {
        category: categorySlug, gender: this.route.snapshot.queryParams['gender']
      }
    })
	}

	onChangeGender (gender: string) {
		this.router.navigate(['/catalog'], {
      queryParams: {
        category: this.route.snapshot.queryParams['category'], gender
      }
    })
  }

}
