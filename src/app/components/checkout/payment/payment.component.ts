import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  @Input('paymentInfo') paymentInfo;
  @Input('progress') progress;
  public cardMask = [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];
  public expMask = [/[0-1]/, /\d/, '/', /\d/, /\d/];
  public cscMask = [/\d/, /\d/, /\d/];

  constructor() { }

  ngOnInit() {
  }

}
