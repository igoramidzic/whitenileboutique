import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../models/item';
import { CartService } from '../../../services/cart.service';

@Component({
  selector: 'app-step-layout',
  templateUrl: './step-layout.component.html',
  styleUrls: ['./step-layout.component.scss']
})
export class StepLayoutComponent implements OnInit {

  cart: Item[] = [];
  @Input('contactInfo') contactInfo;
  @Input('shippingInfo') shippingInfo;
  @Input('paymentInfo') paymentInfo;
  @Input('progress') progress;

  get subTotal () {
    let amount = 0;
    if (this.cart) {
      this.cart.forEach(item => amount += item.price * item.quantity);
    }
    return amount;
  }

  get tax () {
    return this.subTotal * 0.07;
  }

  get shipping () {
    if (this.subTotal > 0) {
      return 5;
    }
    return 0;
  }

  get total () {
    return this.subTotal + this.tax + this.shipping;
  }

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cart = this.cartService.cartItems;
    this.cartService.cart.subscribe(cart => {
      this.cart = cart;
    });
  }

  disabledButton () {
    if (!this.progress[0].complete) {
      if (this.contactInfo.email) {
        return false;
      }
      return true;
    } else if (this.progress[0].complete && !this.progress[1].complete) {
      if (!this.shippingInfo.address1 || !this.shippingInfo.city ||
        !this.shippingInfo.state ||  !this.shippingInfo.zip || !(this.shippingInfo.zip.length === 5)) {
        return true;
      }
    } else if (this.progress[0].complete && this.progress[1].complete && !this.progress[2].complete) {
      if (
        !this.paymentInfo.name || !this.paymentInfo.cardNum ||
        !(this.paymentInfo.cardNum.length === 19) || !this.paymentInfo.expiration || !(this.paymentInfo.expiration.length === 5) || !this.paymentInfo.csc ||
        !(this.paymentInfo.csc.length === 3)) {
        return true;
      }
    }
    return false;
  }

  onNext () {
    if (!this.progress[0].complete) {
      this.progress[0].complete = true;
    } else if (this.progress[0].complete && !this.progress[1].complete) {
      this.progress[1].complete = true;
    } else if (this.progress[0].complete && this.progress[1].complete && !this.progress[2].complete) {
      this.progress[2].complete = true;
    }
    window.scroll(0, 0);
  }

  onBack () {
    if (!this.progress[1].complete || (this.progress[1].complete && !this.progress[0].complete)) {
      this.progress[0].complete = false;
      window.scroll(0, 0);
    }
  }

}
