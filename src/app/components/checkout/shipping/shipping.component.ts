import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {

  @Input('shippingInfo') shippingInfo;
  public zipMask = [/\d/, /\d/, /\d/, /\d/, /\d/];

  constructor() { }

  ngOnInit() {
    this.setDeliveryDate();
  }

  setDeliveryDate () {
    let deliveryDate = new Date();
    let numberOfDaysToAdd = 7;
    deliveryDate.setDate(deliveryDate.getDate() + numberOfDaysToAdd);
    this.shippingInfo.deliveryDate = deliveryDate;
  }

}
