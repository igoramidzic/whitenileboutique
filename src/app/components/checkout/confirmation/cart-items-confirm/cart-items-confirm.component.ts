import { Component, OnInit } from '@angular/core';
import { Item } from '../../../../models/item';
import { CartService } from '../../../../services/cart.service';

@Component({
  selector: 'app-cart-items-confirm',
  templateUrl: './cart-items-confirm.component.html',
  styleUrls: ['./cart-items-confirm.component.scss']
})
export class CartItemsConfirmComponent implements OnInit {

  cart: Item[];

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cart = this.cartService.cartItems;
    this.cartService.cart.subscribe(cart => {
      this.cart = cart;
    });
  }

}
