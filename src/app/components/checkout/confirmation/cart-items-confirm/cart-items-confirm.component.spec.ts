import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartItemsConfirmComponent } from './cart-items-confirm.component';

describe('CartItemsConfirmComponent', () => {
  let component: CartItemsConfirmComponent;
  let fixture: ComponentFixture<CartItemsConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartItemsConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartItemsConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
