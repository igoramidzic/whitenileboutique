import { Component, OnInit, Input } from '@angular/core';
import { CartService } from '../../../services/cart.service';
import { Item } from '../../../models/item';
import { CheckoutService } from '../../../services/checkout.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  @Input('contactInfo') contactInfo;
  @Input('shippingInfo') shippingInfo;
  @Input('paymentInfo') paymentInfo;
  @Input('progress') progress;
  cart: Item[];
  completeInfo: any[];
  orderCompleted: boolean;
  placeOrderInProgress: boolean;

  get subTotal () {
    let amount = 0;
    this.cart.forEach(item => amount += item.price * item.quantity);
    return amount;
  }

  get tax () {
    return this.subTotal * 0.07;
  }

  get shipping () {
    if (this.subTotal > 0) {
      return 5;
    }
    return 0;
  }

  get total () {
    return this.subTotal + this.tax + this.shipping;
  }

  constructor(private cartService: CartService, private checkoutService: CheckoutService) { }

  ngOnInit() {
    this.orderCompleted = false;
    this.placeOrderInProgress = false;

    this.cart = this.cartService.cartItems;
    this.cartService.cart.subscribe(cart => {
      this.cart = cart;
    });

    this.completeInfo = [
      { name: 'Shipping Info', info: this.shippingInfo },
      { name: 'Payment Info', info: this.paymentInfo }
    ];
  }

  onPlaceOrder () {
    this.placeOrderInProgress = true;
    this.checkoutService.placeOrder(this.cart, this.contactInfo, this.shippingInfo, this.paymentInfo)
      .then(res => {
        this.orderCompleted = true;
        this.placeOrderInProgress = false;
        this.cartService.cart.next(null);
      })
      .catch(error => console.log(error));
  }

  onNavToInfo (info) {
    if (info === 'Shipping Info') {
      this.progress[1].complete = false;
    } else if (info === 'Payment Info') {
      this.progress[2].complete = false;
    }
  }

}
