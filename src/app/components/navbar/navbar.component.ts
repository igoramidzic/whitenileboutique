import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Item } from '../../models/item';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output() onToggleSidebar = new EventEmitter<boolean>();
  category: string;
  gender: string;
  cart: Item[];

  constructor(public route: ActivatedRoute, private router: Router,
              private cartService: CartService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.category = params['category'];
      this.gender = params['gender'];
    })

    this.cartService.cart.subscribe(cart => {
      this.cart = cart;
    })
  }

  onRemoveCategory () {
		this.router.navigate(['/catalog'], {
      queryParams: {
        category: null, gender: this.route.snapshot.queryParams['gender']
      }
    })
  }

  onRemoveGender () {
    this.router.navigate(['/catalog'], {
      queryParams: {
        category: this.route.snapshot.queryParams['category'], gender: null
      }
    })
  }

}
