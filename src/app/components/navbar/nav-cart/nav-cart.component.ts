import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../models/item';
import { CartService } from '../../../services/cart.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-nav-cart',
  templateUrl: './nav-cart.component.html',
  styleUrls: ['./nav-cart.component.scss']
})
export class NavCartComponent implements OnInit {

  @Input('cart') cart: Item[];

  get subTotal () {
    let amount = 0;
    this.cart.forEach(item => amount += item.price * item.quantity);
    return amount;
  }

  get tax () {
    return this.subTotal * 0.07;
  }

  get shipping () {
    return 5;
  }

  get total () {
    return this.subTotal + this.tax + this.shipping;
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onNavToCart () {
    this.router.navigate(['cart']);
  }

}
