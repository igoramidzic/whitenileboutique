export interface Item {
	id: number,
	name: string,
	img: string,
	category: string,
	price: number,
	gender: string,
	sizes: string[],
	details: string,
	reviews: {
		rating: number,
		description: string
	}[];
	size: string;
	quantity: number;
};
