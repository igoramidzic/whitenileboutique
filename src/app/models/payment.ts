export interface Payment {
  name: string;
  cardNum: string;
  expiration: string;
  csc: number;
}
