export interface Shipping {
  name: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  zip: number;
  deliveryDate: Date;
}
