import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CatalogComponent } from './pages/catalog/catalog.component';
import { SingleItemComponent } from './pages/single-item/single-item.component';
import { CartComponent } from './pages/cart/cart.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';

const routes: Routes = [
  { path: '', component: MainLayoutComponent, children: [
    { path: '', pathMatch: 'full', component: CatalogComponent },
    { path: 'catalog', component: CatalogComponent},
    { path: 'catalog/product', component: SingleItemComponent },
    { path: 'contact', component: ContactComponent }
  ] },
  { path: 'cart', component: CartComponent },
  { path: 'checkout', component: CheckoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
