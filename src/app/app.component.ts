import { Component } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  mac: boolean;

  constructor (private deviceService: DeviceDetectorService) {
    this.mac = (this.deviceService.getDeviceInfo().os == 'mac' ? true : false);
  }
}
