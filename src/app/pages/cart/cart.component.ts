import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/item';
import { CartService } from '../../services/cart.service';
import { CatalogService } from '../../services/catalog.service';
import { Router } from '../../../../node_modules/@angular/router';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cart: Item[];

  get itemsAmount () {
    let amount = 0;
    this.cart.forEach(item => amount += item.quantity);
    return amount;
  }

  get subTotal () {
    let amount = 0;
    this.cart.forEach(item => amount += item.price * item.quantity);
    return amount;
  }

  get tax () {
    return this.subTotal * 0.07;
  }

  get shipping () {
    if (this.subTotal > 0) {
      return 5;
    }
    return 0;
  }

  get total () {
    return this.subTotal + this.tax + this.shipping;
  }

  user: any;

  constructor(private cartService: CartService,
              private router: Router,
              private inventoryService: CatalogService,
              private modalService: ModalService) { }

  ngOnInit() {
    this.cart = this.cartService.cartItems;
    this.cartService.cart.subscribe(cart => {
      this.cart = cart;
    });
  }

  onRemove (index) {
    this.cartService.removeItemFromCart(index);
  }

  onUpdateCartItemQuantity (index, quantity) {
    this.cartService.updateCartItemQuantity(index, quantity);
  }

  onCheckoutGuest () {
    this.router.navigate(['checkout']);
  }

  onCheckoutPaypal () {
    this.router.navigate(['checkout']);
  }

}
