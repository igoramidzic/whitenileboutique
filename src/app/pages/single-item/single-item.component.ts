import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../../services/catalog.service';
import { Item } from '../../models/item';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-single-item',
  templateUrl: './single-item.component.html',
  styleUrls: ['./single-item.component.scss']
})
export class SingleItemComponent implements OnInit {

  item: Item;

  constructor(private catalogService: CatalogService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.item = {
      id: 0,
      name: 'Loading',
      img: 'assets/img/0.jpg',
      category: '',
      price: 0.00,
      gender: null,
      sizes: ['-', '-', '-', '-'],
      details: 'Details',
      reviews: [],
      size: '',
      quantity: 1
    };

    this.getItemDetails();

    this.route.queryParams.subscribe(params => {
      this.getItemDetails();
    })
  }

  getItemDetails () {
    if (this.route.snapshot.queryParams['id']) {
      this.catalogService.getOneItem(this.route.snapshot.queryParams['id'])
      .then((item: Item) => {
        this.item = item;
      })
    }
  }

}
