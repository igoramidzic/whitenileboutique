import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { CartService } from '../../services/cart.service';
import { Payment } from '../../models/payment';
import { Shipping } from '../../models/shipping';
import { Contact } from '../../models/contact';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  progress: progress[];
  contactInfo: Contact;
  shippingInfo: Shipping;
  paymentInfo: Payment;

  constructor(private router: Router,
              private cartService: CartService) { }

  ngOnInit() {

    if (this.cartService.cartItems.length == 0) {
      this.router.navigate(['cart']);
    }

    this.progress = [
      { name: 'contact', complete: false },
      { name: 'shipping', complete: false },
      { name: 'payment', complete: false },
      { name: 'confirmation', complete: false }
    ];

    this.contactInfo = {
      email: null
    }

    this.shippingInfo = {
      name: null,
      address1: null,
      address2: null,
      city: null,
      state: null,
      zip: null,
      deliveryDate: null
    };

    this.paymentInfo = {
      name: null,
      cardNum: null,
      expiration: null,
      csc: null
    };
  }

}

interface progress {
  name: string;
  complete: boolean;
}
