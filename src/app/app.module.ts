import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { ClipboardModule } from 'ngx-clipboard';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import {enableProdMode} from '@angular/core';

enableProdMode();

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { NgPipesModule, SlugifyPipe } from 'ngx-pipes';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment.prod';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { CatalogComponent } from './pages/catalog/catalog.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavCartComponent } from './components/navbar/nav-cart/nav-cart.component';
import { CatalogListComponent } from './components/catalog-list/catalog-list.component';
import { CatalogItemComponent } from './components/catalog-list/catalog-item/catalog-item.component';
import { ItemModalComponent } from './components/item-modal/item-modal.component';
import { ModalService } from './services/modal.service';
import { MainItemInfoComponent } from './components/main-item-info/main-item-info.component';
import { SingleItemComponent } from './pages/single-item/single-item.component';
import { CartComponent } from './pages/cart/cart.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { StepLayoutComponent } from './components/checkout/step-layout/step-layout.component';
import { ShippingComponent } from './components/checkout/shipping/shipping.component';
import { PaymentComponent } from './components/checkout/payment/payment.component';
import { ConfirmationComponent } from './components/checkout/confirmation/confirmation.component';
import { CartItemsConfirmComponent } from './components/checkout/confirmation/cart-items-confirm/cart-items-confirm.component';
import { CompletedOrderComponent } from './components/checkout/completed-order/completed-order.component';
import { ContactInfoComponent } from './components/checkout/contact-info/contact-info.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    MainLayoutComponent,
    CatalogComponent,
    ContactComponent,
    NavbarComponent,
    NavCartComponent,
    CatalogListComponent,
    CatalogItemComponent,
    ItemModalComponent,
    MainItemInfoComponent,
    SingleItemComponent,
    CartComponent,
    CheckoutComponent,
    StepLayoutComponent,
    ShippingComponent,
    PaymentComponent,
    ConfirmationComponent,
    CartItemsConfirmComponent,
    CompletedOrderComponent,
    ContactInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
		AngularFirestoreModule,
    AngularFireAuthModule,
    MatSidenavModule,
    NgPipesModule,
    MatTooltipModule,
    MatButtonModule,
    DeviceDetectorModule.forRoot(),
    ClipboardModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule
  ],
  entryComponents: [
    ItemModalComponent
	],
  providers: [SlugifyPipe, ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
